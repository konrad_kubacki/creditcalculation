﻿using CreditCalculation.Domain.Enums;

namespace CreditCalculation.Domain.Factory.Client
{
    public interface IClientFactory
    {
        Entity.Client.Client Create(ClientType clientType);
    }
}