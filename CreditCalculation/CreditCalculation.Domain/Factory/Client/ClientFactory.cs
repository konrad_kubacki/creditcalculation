﻿using System;
using CreditCalculation.Domain.Entity.Client;
using CreditCalculation.Domain.Enums;

namespace CreditCalculation.Domain.Factory.Client
{
    public class ClientFactory : IClientFactory
    {
        public Entity.Client.Client Create(ClientType clientType)
        {
            Entity.Client.Client client = null;
            switch (clientType)
            {
                case ClientType.PhysicalPerson:
                    client = new PhysicalPerson();
                    client.Type = ClientType.PhysicalPerson;
                    break;
                case ClientType.Company:
                    client = new Company();
                    client.Type = ClientType.Company;
                    break;
                default: throw new ArgumentException("Given wrong client type");
            }

            return client;
        }
    }
}