﻿using System.ComponentModel;

namespace CreditCalculation.Domain.Enums
{
    public enum ClientType
    {
        [Description("Company")] Company = 1,

        [Description("Physical Person")] PhysicalPerson = 2
    }
}