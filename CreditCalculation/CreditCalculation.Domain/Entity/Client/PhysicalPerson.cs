﻿namespace CreditCalculation.Domain.Entity.Client
{
    class PhysicalPerson : Client
    {
        public override double CalculateAnualFeeAmount()
        {
            double sum = 0;
            Credits.ForEach(c => { sum += CalculateAnualFeeAmount(c); });
            return sum;
        }

        public override double CalculateAnualFeeReduction()
        {
            return Credits.Count * 400;
        }

        private double CalculateAnualFeeAmount(Credit.Credit credit)
        {
            //I left this in such state because IMO at this time we dont need another implementation.
            if (credit.Amount > 80000)
            {
                return credit.Amount * 0.1;
            }

            return credit.Amount * 0.18;
        }
    }
}
