﻿using System;

namespace CreditCalculation.Domain.Entity.Client.Credit
{
    public class Credit : IEntity
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public double Amount { get; set; }

        public Credit()
        {
        }

        public Credit(Guid clientId, double amount)
        {
            ClientId = clientId;
            Amount = amount;
        }
    }
}