﻿using System;
using System.Collections.Generic;
using CreditCalculation.Domain.Enums;

namespace CreditCalculation.Domain.Entity.Client
{
    public abstract class Client : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ClientType Type { get; set; }
        public List<Credit.Credit> Credits { get; set; } = new List<Credit.Credit>();
        public abstract double CalculateAnualFeeAmount();
        public abstract double CalculateAnualFeeReduction();
    }
}