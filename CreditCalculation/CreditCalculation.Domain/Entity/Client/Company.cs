﻿using System.Linq;

namespace CreditCalculation.Domain.Entity.Client
{
    class Company : Client
    {
        public override double CalculateAnualFeeAmount()
        {
            return 0.1 * Credits.Sum(c => c.Amount);
        }

        public override double CalculateAnualFeeReduction()
        {
            return 1000;
        }
    }
}