﻿using System;

namespace CreditCalculation.Domain.Entity
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
