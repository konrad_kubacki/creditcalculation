﻿using System;
using System.Linq;
using CreditCalculation.Domain.Entity.Client.Credit;
using CreditCalculation.Domain.Enums;
using CreditCalculation.Domain.Factory.Client;
using CreditCalculation.Infrastructure.Repository;
using CreditCalculation.Service.Client;
using Xunit;

namespace CreditCalculation.Service.Tests.Client
{
    //We can use moq for mocking repository, but i dont have enought time to do that :)
    //of cource we can write tests for methods wich left.
    public class ClientServiceTestSimpleRepository
    {
        [Fact]
        public void Can_Add_Client_Company()
        {
            //GIVEN
            var target = new ClientService(new SimpleRepository<Domain.Entity.Client.Client>(),
                new SimpleRepository<Credit>(), new ClientFactory());

            //WHEN
            var insertedClient = target.AddClient(ClientType.Company, "Company client");

            //THEN
            Assert.True(insertedClient.Id != default(Guid));
            Assert.Equal(ClientType.Company, insertedClient.Type);
            Assert.Equal("Company client", insertedClient.Name);
        }

        [Fact]
        public void Can_Add_Client_PhysicalPerson()
        {
            //GIVEN
            var target = new ClientService(new SimpleRepository<Domain.Entity.Client.Client>(),
                new SimpleRepository<Credit>(), new ClientFactory());

            //WHEN
            var insertedClient = target.AddClient(ClientType.PhysicalPerson, "Physical person client");

            //THEN
            Assert.True(insertedClient.Id != default(Guid));
            Assert.Equal(ClientType.PhysicalPerson, insertedClient.Type);
            Assert.Equal("Physical person client", insertedClient.Name);
        }

        //I know it is not Unit test, but i have wrote this to be sure whether the logic is good.
        [Fact]
        public void Company_Have_Two_Credits()
        {
            //GIVEN
            var target = new ClientService(new SimpleRepository<Domain.Entity.Client.Client>(),
                new SimpleRepository<Credit>(), new ClientFactory());
            var insertedClient = target.AddClient(ClientType.Company, "Company client");
            var insertedCredit0 = target.AddCredit(insertedClient.Id, 50000);
            var insertedCredit1 = target.AddCredit(insertedClient.Id, 40000);

            //WHEN
            var insertedClientViewModel = target.GetAllClients().FirstOrDefault();

            //Then
            Assert.Equal(8000, insertedClientViewModel?.TotalFees);
            Assert.Equal(ClientType.Company.GetDescription(), insertedClientViewModel?.Type);
            Assert.Equal("Company client", insertedClientViewModel?.Name);
        }

        //I know it is not Unit test, but i have wrote this to be sure whether the logic is good.
        [Fact]
        public void PhysicalPerson_Have_Two_Credits()
        {
            //GIVEN
            var target = new ClientService(new SimpleRepository<Domain.Entity.Client.Client>(),
                new SimpleRepository<Credit>(), new ClientFactory());
            var insertedClient = target.AddClient(ClientType.PhysicalPerson, "Physical person");
            var insertedCredit0 = target.AddCredit(insertedClient.Id, 50000);
            var insertedCredit1 = target.AddCredit(insertedClient.Id, 40000);

            //WHEN
            var insertedClientViewModel = target.GetAllClients().FirstOrDefault();

            //Then
            Assert.Equal(15400, insertedClientViewModel?.TotalFees);
            Assert.Equal(ClientType.PhysicalPerson.GetDescription(), insertedClientViewModel?.Type);
            Assert.Equal("Physical person", insertedClientViewModel?.Name);
        }
    }
}