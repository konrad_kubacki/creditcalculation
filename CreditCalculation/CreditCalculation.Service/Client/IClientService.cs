﻿using System;
using System.Collections.Generic;
using CreditCalculation.Domain.Entity.Client.Credit;
using CreditCalculation.Domain.Enums;
using CreditCalculation.Service.ViewModels;

namespace CreditCalculation.Service.Client
{
    public interface IClientService
    {
        List<ClientViewModel> GetAllClients();
        Domain.Entity.Client.Client AddClient(ClientType clientType,string name);
        Credit AddCredit(Guid clientId, double amount);
    }
}
