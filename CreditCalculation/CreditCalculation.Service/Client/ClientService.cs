﻿using System;
using System.Collections.Generic;
using System.Linq;
using CreditCalculation.Domain.Entity.Client.Credit;
using CreditCalculation.Domain.Enums;
using CreditCalculation.Domain.Factory.Client;
using CreditCalculation.Infrastructure.Repository;
using CreditCalculation.Service.ViewModels;

namespace CreditCalculation.Service.Client
{
    //To do: add asynch methods for long runnig task for example 
    //create new method GetAllClientsAsynch()
    public class ClientService : IClientService
    {
        private readonly IRepository<Domain.Entity.Client.Client> _clientRepository;
        private readonly IRepository<Domain.Entity.Client.Credit.Credit> _creditRepository;
        private readonly IClientFactory _clientFactory;

        public ClientService(IRepository<Domain.Entity.Client.Client> clientRepository,
            IRepository<Domain.Entity.Client.Credit.Credit> creditRepository,
            IClientFactory clientFactory)
        {
            _clientRepository = clientRepository;
            _creditRepository = creditRepository;
            _clientFactory = clientFactory;
        }

        public List<ClientViewModel> GetAllClients()
        {
            var clientsEntities = _clientRepository.GetAll();
            var clientViewModels = new List<ClientViewModel>();
            foreach (var client in clientsEntities)
            {
                client.Credits = _creditRepository.GetList(c => c.ClientId.Equals(client.Id)).ToList();
                var clientViewModel = new ClientViewModel
                {
                    Id = client.Id,
                    Name = client.Name,
                    Type = client.Type.GetDescription(),
                    TotalFees = client.CalculateAnualFeeAmount() - client.CalculateAnualFeeReduction()
                };
                clientViewModels.Add(clientViewModel);
            }

            return clientViewModels;
        }

        public Domain.Entity.Client.Client AddClient(ClientType clientType, string name)
        {
            var client = _clientFactory.Create(clientType);
            client.Name = name;
            _clientRepository.Insert(client);

            return client;
        }

        public Credit AddCredit(Guid clientId, double amount)
        {
            var credit = new Credit(clientId, amount);
            _creditRepository.Insert(credit);

            return credit;
        }
    }
}
