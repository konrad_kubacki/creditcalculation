﻿using System;

namespace CreditCalculation.Service.ViewModels
{
    public class ClientViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public double TotalFees { get; set; }
    }
}