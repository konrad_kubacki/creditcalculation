﻿using System;

namespace CreditCalculation.Service.ViewModels
{
    public class CreateCreditViewModel
    {
        public Guid ClientId { get; set; }
        public double Amount { get; set; }
    }
}
