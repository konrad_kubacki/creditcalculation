﻿namespace CreditCalculation.Service.ViewModels
{
    public class CreateClientViewModel
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public double CreditAmount { get; set; }
    }
}
