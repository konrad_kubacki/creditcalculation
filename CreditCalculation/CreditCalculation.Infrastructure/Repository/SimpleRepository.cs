﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CreditCalculation.Domain.Entity;

namespace CreditCalculation.Infrastructure.Repository
{
    //We can add Entity framework Context and Unit of work pattern.
    //Because of that I did not implemented Update method.
    public class SimpleRepository<T> : IRepository<T> where T : class, IEntity
    {
        private List<T> _entitiesContainer;

        public SimpleRepository()
        {
            _entitiesContainer = new List<T>();
        }

        public Guid Insert(T entity)
        {
            entity.Id = Guid.NewGuid();
            _entitiesContainer.Add(entity);

            return entity.Id;
        }

        public void Delete(T entity)
        {
            _entitiesContainer.Remove(entity);
        }

        public List<T> GetAll()
        {
            return _entitiesContainer.GetRange(0, _entitiesContainer.Count);
        }

        public T GetById(Guid id)
        {
            return _entitiesContainer.SingleOrDefault(t => t.Id.Equals(id));
        }

        public IQueryable<T> GetList(Expression<Func<T, bool>> predicate)
        {
            return _entitiesContainer.AsQueryable().Where(predicate);
        }
    }
}