﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CreditCalculation.Domain.Entity;

namespace CreditCalculation.Infrastructure.Repository
{
    public interface IRepository<T> where T : class, IEntity
    {
        Guid Insert(T entity);
        void Delete(T entity);
        List<T> GetAll();
        T GetById(Guid id);
        IQueryable<T> GetList(Expression<Func<T, bool>> predicate);
    }
}