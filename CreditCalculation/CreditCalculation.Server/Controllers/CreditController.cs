﻿using System;
using CreditCalculation.Service.Client;
using CreditCalculation.Service.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CreditCalculation.Server.Controllers
{
    [Route("api/[controller]")]
    public class CreditController : Controller
    {
        private readonly IClientService _clientService;

        public CreditController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpPost("")]
        public IActionResult Create([FromBody] CreateCreditViewModel createClient)
        {
            try
            {
                var credit = _clientService.AddCredit(createClient.ClientId, createClient.Amount);

                return StatusCode(StatusCodes.Status201Created, credit);
            }

            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }
    }
}