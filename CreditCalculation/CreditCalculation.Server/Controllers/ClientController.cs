﻿using System;
using CreditCalculation.Domain.Enums;
using CreditCalculation.Service.Client;
using CreditCalculation.Service.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CreditCalculation.Server.Controllers
{
    [Route("api/[controller]")]
    public class ClientController : Controller
    {
        //To do : add asynch methods for long running task for example for Index() action.
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpGet("Index")]
        public IActionResult Index()
        {
            try
            {
                return Ok(_clientService.GetAllClients());
            }

            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        [HttpPost("")]
        public IActionResult Create([FromBody]CreateClientViewModel createClient)
        {
            try
            {
                var client = _clientService.AddClient((ClientType) createClient.Type, createClient.Name);

                return StatusCode(StatusCodes.Status201Created, client);
            }

            catch (System.ArgumentException e)
            {
                return BadRequest(e);
            }

            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }
    }
}