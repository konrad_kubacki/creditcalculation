using System.Diagnostics;
using CreditCalculation.Service.Client;
using Microsoft.AspNetCore.Mvc;

namespace CreditCalculation.Server.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
