import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { ClientList } from './components/ClientList';
import { AddClient } from './components/AddClient';
import { AddCredit } from './components/AddCredit';


export const routes = <Layout>
    <Route exact path='/' component={ClientList} />
    <Route exact path='/clientlist' component={ClientList} />
    <Route path='/addclient' component={AddClient} />
    <Route path='/credit/add/:clientid/:clientname' component={AddCredit} />
</Layout>;
