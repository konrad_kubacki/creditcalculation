﻿export class ClientViewModel {
    id: string = "";
    name: string = "";
    type: string = "";
    totalFees: number = 0;
} 