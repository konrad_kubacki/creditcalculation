﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { CreateCreditViewModel }  from '../Models/CreateCreditViewModel';

interface AddCreditDataState {
    title: string;
    clientName:string;
    credit: CreateCreditViewModel;
}

export class AddCredit extends React.Component<RouteComponentProps<{}>, AddCreditDataState> {
    constructor(props) {
        super(props);

        var creditVal = new CreateCreditViewModel();
        creditVal.clientId = this.props.match.params["clientid"];
        var clientNameVal = decodeURIComponent(this.props.match.params["clientname"]);

        this.state = { title: "Add credit", clientName:clientNameVal ,credit: creditVal };
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    public render() {
        let contents = this.renderCreateForm();
        return <div>
            <h1>{this.state.title}</h1>
            <h3>Client: {this.state.clientName}</h3>
            <hr />
            {contents}
        </div>;
    }

    private handleSave(event) {
        event.preventDefault();
        //Mayby this not the best code in react but keep in mind it is my first contact with react code,
        //so I decided to left code as it is because of lack of time.
        fetch('api/Credit',
            {
                method: 'POST',
                body: JSON.stringify({
                    clientId: event.target[0].value,
                    amount: event.target[1].value
                }),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            }).then((response) => {
            //We can add some logging info if something went wrong.
            this.props.history.push("/clientlist");
        });
    }

    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/clientlist");
    }

    private renderCreateForm() {
        return (
            <form onSubmit={this.handleSave}>
                <div className="form-group row">
                    <input type="hidden" name="clientId" value={this.state.credit.clientId} />
                </div>
                <div className="form-group row">
                    <label className=" control-label col-md-12" htmlFor="amount">Credit Amount</label>
                    <div className="col-md-4">
                        <input className="form-control" step="0.01" type="number" name="amount" defaultValue={this.state.credit
                            .amount} required />
                    </div>
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div>
            </form>
        )
    }
}