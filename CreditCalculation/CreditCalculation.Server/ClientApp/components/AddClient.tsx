﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { CreateClientViewModel } from '../Models/CreateClientViewModel'

interface AddClientDataState {
    title: string;
    client: CreateClientViewModel;
}
//To do: add two way data binding something like we have in angular 2 rxjs.
//I dont think run react backend from visual studio is good idea, so I have to check how run react backend from command line :)
export class AddClient extends React.Component<RouteComponentProps<{}>, AddClientDataState> {
    constructor(props) {
        super(props);
        this.state = { title: "Add Client", client: new CreateClientViewModel };

        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    public render() {
        let contents = this.renderCreateForm();
        return <div>
                   <h1>{this.state.title}</h1>
                   <hr/>
                   {contents}
               </div>;
    }

    private handleSave(event) {
        event.preventDefault();
        //Mayby this not the best code in react but keep in mind it is my first contact with react code,
        //so I decided to left code as it is because of lack of time.
        var clientToSend = {
            name: event.target[0].value,
            creditAmount: event.target[1].value,
            type: event.target[2].value,
        };

        fetch('api/Client',
            {
                method: 'POST',
                body: JSON.stringify(clientToSend),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },

            }).then((response) => {

            if (response.ok) {
                response.json().then(json => {
                    var clientIdVal = json.id;
                    fetch('api/Credit',
                        {
                            method: 'POST',
                            body: JSON.stringify({
                                clientId: clientIdVal,
                                amount: clientToSend.creditAmount
                            }),
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        }).then((response) => {
                        //We can add some logging info if something went wrong.
                        this.props.history.push("/clientlist");
                    });

                });
            }
        });
    }

    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/clientlist");
    }

    private renderCreateForm() {
        return (
            <form onSubmit={this.handleSave}>
                <div className="form-group row">
                    <label className=" control-label col-md-12" htmlFor="name">Name</label>
                    <div className="col-md-4">
                        <input className="form-control" ref="name" type="text" name="name" defaultValue={this.state
                            .client.name
} required/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className=" control-label col-md-12" ref="creditAmount" htmlFor="creditAmount">Credit Amount</label>
                    <div className="col-md-4">
                        <input className="form-control" step="0.01" min="0" type="number" name="creditAmount" defaultValue={
this.state.client.creditAmount} required/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="type">Type</label>
                    <div className="col-md-4">
                        <select className="form-control" ref="type" data-val="true" name="type" defaultValue={this
                            .state.client
                            .type} required>
                            <option value="">-- Select Type --</option>
                            <option value="1">Company</option>
                            <option value="2">Physical Person</option>
                        </select>
                    </div>
                </div >
                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div>
            </form>
        )
    }
}
