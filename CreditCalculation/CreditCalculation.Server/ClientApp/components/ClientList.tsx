﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { ClientViewModel } from "../Models/ClientViewModel";

interface ClientListDataState {
    clients: ClientViewModel[];
    loading: boolean;
}

export class ClientList extends React.Component<RouteComponentProps<{}>, ClientListDataState> {
    constructor() {
        super();
        this.state = { clients: [], loading: true };

        fetch('api/Client/Index',
                {
                    method: 'get',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                })
            .then(response => response.json() as Promise<ClientViewModel[]>)
            .then(data => {
                this.setState({ clients: data, loading: false });
            });

        this.handleAddCredit = this.handleAddCredit.bind(this);
    }

    public render() {
        let contents = this.state.loading
            ? <p>
                  <em>Loading...</em>
              </p>
            : this.renderClientsTable(this.state.clients);

        return <div>
                   <h1>Client list</h1>
                   <p>
                       <Link to="/addclient">Create New</Link>
                   </p>
                   {contents}
               </div>;
    }

    private handleAddCredit(clientId: string, clientName: string) {
        this.props.history.push("/credit/add/" + clientId + "/" + clientName);
    }

    private renderClientsTable(clients: ClientViewModel[]) {
        return <table className='table'>
                   <thead>
                   <tr>
                       <th></th>
                       <th>Id</th>
                       <th>Name</th>
                       <th>Type</th>
                       <th>Total Fees</th>
                       <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   {clients.map(c =>
                       <tr key={c.id}>
                           <td></td>
                           <td>{c.id}</td>
                           <td>{c.name}</td>
                           <td>{c.type}</td>
                           <td>{c.totalFees}</td>
                           <td>
                               <a className="action" onClick={(id) => this.handleAddCredit(c.id, c.name)}>Add credit</a>
                           </td>
                       </tr>
                   )}
                   </tbody>
               </table>;
    }
}
